https://openbeta.substack.com/p/openbeta-vs-mountainproject-vs-thecrag

https://nextascent.org/

- [Climb-On Maps](https://www.climbonmaps.com/)
  - general maps for an area
  - nice pie charts for areas
  - approach & descent directions.
    very good for areas like Red Rocks and Joshua tree

- gym tools
  - [toplogger](https://toplogger.nu/) for gym guides/records/setters
  - https://www.climbconnect.com/
  - https://www.problemator.fi/

- https://pinnacleclimb.com/
  for climbing workout logging

- [fontNbleau](https://play.google.com/store/apps/details?id=com.fontNbleau.guide&hl=en_US)
  area-specific app

- betacreator
  - https://sites.google.com/site/betacreator/
  - https://www.climbing.com/news/betacreator-new-tool-for-making-perfect-climbing-topos/

- betaflash
  - https://sites.google.com/site/betaflashsite/

### weather

- http://climbingweather.com/
- https://itunes.apple.com/us/app/where-to-climb/id1235937478

## other

- https://www.pebbleclimbing.com/
- www.bergsteigen.com	Alpine/crags/via ferrata/ice, great topos of routes in DE/AT/CH/IT
- www.supertopo.com	western US only, good database, active forum, useful recommended gear tab, only limited free topos
- www.bcclimbing.com	BC, CA
- www.arcowall.com	Sarca Valley, IT
- www.ogawayama.com	Ogawayama, JP
- http://javu.co.uk	SW England
- www.redriverclimbing.com/RRCGuide/	Red River Gorge, US
- www.escalade-74.com	Haute-Savoie, FR
- http://TopoHawk.com/ 
