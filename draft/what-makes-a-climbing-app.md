---
title: "What Makes a Climbing App?"
publishDate: 2019-07-20
lastmod: 2019-07-20
draft: true
---

## what is a climb?

## what's the threshold for "publishing"?

## where does value come from?

- rakkup focuses on directions
- firstascent optimizes uploads
- boltline ["gigapixel-scale topos"](https://www.boltline.org/about)
- the crag focuses on tons of data, sacrificing quality

## how are climbs grouped?

## where does the beta come from?

## beyond rocks

### training, ticks, and tracking

## how is it distributed?

- free
- subscribe to the service
- subscribe to an area
- buy an area

### free

- pros
  - everything
- cons
  - less likely to find the highest quality beta

### buy an area

- rakkup does this

### subscribe to the service

### subscribe to an area
