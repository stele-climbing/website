#!/bin/sh

convert \
	./boulder.jpg \
	\( \
		-size 1067x800 \
		xc: \
		-strokewidth 10 \
		-fill None \
		-stroke black \
	        +noise Random \
		-draw 'stroke-linecap round stroke-linejoin round polyline 80,100 500,700 750,50' \
		-blur 0x8 \
		-threshold 35% \
	        -transparent white \
        \) \
	-flatten \
	overlay.png

exit 

convert \
	./boulder.jpg \
	\( \
		-size 1067x800 \
		xc: \
		-strokewidth 10 \
		-fill None \
		-stroke '#FFFF00AA' \
		-draw 'polyline 80,100 500,700 750,50' \
		-blur 0x5 \
		-threshold 50% \
	\) \
	-flatten \
	blurred.jpg

# boulder.jpg JPEG 1067x800 1067x800+0+0 8-bit sRGB 661727B 0.000u 0:00.000
