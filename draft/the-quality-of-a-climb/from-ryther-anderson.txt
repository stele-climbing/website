see [Estimating Route Quality: Analysis and Applications](https://openbeta.substack.com/p/estimating-route-quality-analysis)

>   0 Star:
>           A truly terrible route, extremely chossy, covered in plant life, a blight on humanity, etc.
>           Example: Reds
>   1 Star:
>           Not particularly enjoyable, somewhat chossy, needs some cleaning, short, too near another line, etc.
>           Example: Low Priest
>   2 Star:
>           Good and worth doing, quality rock, clean, maybe not consistently fun movement.
>           Example: Chaos
>   3 Star:
>           Area classic, one the of best routes in the vicinity
>           (note that this does not apply if all the routes in the vicinity are terrible, use your judgement).
>           Excellent rock, clean, great movement. Example: Reefer Madness
>   4 Star:
>           Classic, one of the best routes anywhere, pristine, beautiful, striking movement, various superlatives, guide-book-cover nonsense.
>           Example: Sonic Youth

and the guidelines

>    Strive for a certain amount of objectivity
>     Ratings are personal.
>     But they should not be influenced by external factors, such as your mood, the weather, the strength of your fingers, etc.
>     FAs should rate their routes very carefully, if at all.
>     Take, for example, Spark Seeker an unfortunate contribution to an excellent crag in Wild Iris.
>     The two FAs gave 4 and 3 stars, however, half of the subsequent ratings are 0-star.
>     Clearly, it is not one of the best routes in the country, Wild Iris, or even Lower Remuda, as the FAs seem to think.
>     Also, if you only like filthy, moist offwidths and think that striking, sustained face climbs on bullet limestone are contemptible, probably avoid rating certain types of routes.
>
>   You must send the route before rating it
>     Mostly, we are talking about free climbs.
>     So, even if you took a 1-second hang, you did an aid version of the climb, which is not the route you intended to rate.
>     Obviously, this doesn't apply if you are doing an aid climb.
>     I would guess that another common source of non-send ratings is TR ascents.
>     If the route has lead bolts, then the FA intended it to be led, so a clean TR burn is not a send.
>
>   Safety does not matter for trad, safety should be considered (with caveats) for sport climbs
>     In a way, when you rate routes you are assessing the work done by the FA (or equipper, I will assume they are the same person).
>     For trad routes, the FA can't alter natural gear placements.
>     I guess they can, but if they do, please rate the route 0-stars.
>     So, there is no point in considering safety unless you want to argue with geological forces.
>     On the other hand, for sport climbs, safe bolting is key to the climbing experience.
>     If a route is rap-bolted there is no good reason to have unsafe runouts, and this should be factored into the rating.
>     A 4-star climb could be ruined by having an unnecessary runout with ground-fall potential or above a ledge.
>     However, I think an objectively safe runout that may feel scary should not count against the climb.
>     If a route is bolted on lead, the criteria are different; dangerous runouts often can't be avoided and this should be taken into consideration.
>
>   Only consider the climb itself
>     A heinous hike through snake-infested cacti does not make the route worse.
>     A loose, 6-inch belay with snake-infested cacti behind it is your belayer's problem.
>     Road noise is not the fault of the FA, it is the fault of the engineers who decided to preemptively ruin the crag by putting a road next to it.
>     However, for sport routes (related to item 3), bolting is part of the climb.
>     Even if the bolting is safe, strange bolt placements can make a climb worse.
>     For example, having a difficult clip in the middle of the crux that cannot be safely skipped and that could be safely located after the crux is a mistake by the FA.
>
>   Consider your experience
>     Have you only climbed at one local crag? If so, you cannot know what a 4-star route is (by definition).
>     Try to climb routes of various ratings in various locations before you start rating climbs.
>     This will also help with item 1.
>
>   Don't be afraid to disagree, but only if you follow the above guidelines
>     Fundamentally, people will have different opinions.
>     In the end, try to follow the above guidelines and just be honest.
 
