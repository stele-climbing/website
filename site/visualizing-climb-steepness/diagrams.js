'use strict';

class SteepnessSegments {
  setSegments(segments){
    const d = segments;
    const decorated = [];
    for (let i = 0; i < d.length; i++) { 
      const isFirst = i === 0;
      const isLast = i === (d.length-1);
      const start = d[i].start;
      const degreesOverhung = d[i].degreesOverhung;
      const stop = isLast ? 1 : d[i+1].start;
      const midpoint = (start + stop) / 2;
      const length = stop - start;
      const radiansOverhung = degreesOverhung * (Math.PI / 180);
      const rise = Math.cos(radiansOverhung);
      const run = Math.sin(radiansOverhung);
      decorated.push({
        start, midpoint, stop, length,
        degreesOverhung, radiansOverhung,
        isFirst, isLast,
        rise, run
      });
    }
    this._segments = decorated;
  }
  getRootElement(){ return this.svg; }
  getSegments(){
    return this._segments.map(({...a}) => ({...a}));
  }
}

export class Lines extends SteepnessSegments {
  constructor(segments) {
    super();
    this.setSegments(segments);
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const vl = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    svg.appendChild(vl);
    this.vl = vl;
    this.svg = svg;
    this.update();
  }

  update(){
    const [w, h] = [1, 1]; // aspect ratio
    const width = 200;
    const height = width * (h / w);
    const svg = this.svg;
    const vl = this.vl;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');
    vl.setAttributeNS(null, 'x1', width / 2);
    vl.setAttributeNS(null, 'y1', '0');
    vl.setAttributeNS(null, 'x2', width / 2);
    vl.setAttributeNS(null, 'y2', height);
    vl.setAttributeNS(null, 'stroke', '#ddd');
    vl.setAttributeNS(null, 'stroke-width', width * 0.02);

    const segs = this.getSegments();
    const kids = svg.querySelectorAll('line.steepness-segment');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];

      if (!seg) {
        kid.remove()
        continue;
      }

      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        kid.setAttributeNS(null, 'class', 'steepness-segment');
        this.svg.appendChild(kid);
      }

      const gap = height * 0.001;
      const south = height - (height * seg.start) - gap;
      const north = height - (height * seg.stop) + gap;
      const center = (north + south) / 2;
      const segLength = north - south;
      const radians = seg.radiansOverhung;
      const run = seg.run * segLength;
      const rise = seg.rise * segLength;

      const centerX = width * 0.5;
      const centerY = center;

      const southX = centerX - (run / 2);
      const southY = centerY - (rise / 2);
      const northX = centerX + (run / 2);
      const northY = centerY + (rise / 2);

      kid.setAttributeNS(null, 'x1', southX);
      kid.setAttributeNS(null, 'y1', southY);
      kid.setAttributeNS(null, 'x2', northX);
      kid.setAttributeNS(null, 'y2', northY);
      kid.setAttributeNS(null, 'stroke', 'black');
      kid.setAttributeNS(null, 'stroke-width', (width * 0.05));
      kid.setAttributeNS(null, 'fill', 'black');
    }
  }
}


export class Polyline extends SteepnessSegments {
  constructor(segments) {
    super();
    this.setSegments(segments);
    this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.polyline = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
    this.svg.appendChild(this.polyline);
    this.update();
  }

  update(){
    const size = 200;
    const width = size;
    const height = size;
    const svg = this.svg;
    const polyline = this.polyline;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');

    let rts = [[0, 0]]; // relative to start
    let north = 0;
    let south = 0;
    let east = 0;
    let west = 0;

    for (let seg of this.getSegments()) {
      const x1 = rts[rts.length-1][0];
      const y1 = rts[rts.length-1][1];
      const run = seg.run * seg.length * size;
      const rise = seg.rise * seg.length * size;
      const x2 = x1 + run;
      const y2 = y1 + rise;
      north = Math.max(north, y2);
      south = Math.min(south, y2);
      east = Math.max(east, x2);
      west = Math.min(west, x2);
      rts.push([x2, y2]);
    }
    const overallSouth = (height/2) - ((north-south)/2);
    const overallWest = (width/2) - ((east-west)/2);
    let points = [];
    for (let [x, y] of rts) {
      let svgX = overallWest + (x-west);
      let svgY = overallSouth + (y-south);
      points.push([width - svgX, height - svgY]);
    }
    polyline.setAttributeNS(null, 'points', points.map(pt=> pt.join(',')).join(' '));
    polyline.setAttributeNS(null, 'style', 'fill:none;stroke-width:3;stroke:black');
  }
}

export class Semicircles extends SteepnessSegments {
  static _idCounter = 1000;
  static getDiagramId(){
    const id = this._idCounter;
    this._idCounter += 1;
    return id;
  }
  constructor(segments) {
    super();
    this.setSegments(segments);
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const vl = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    const defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
    svg.appendChild(defs);
    svg.appendChild(vl);
    this.id = this.constructor.getDiagramId();
    this.defs = defs;
    this.vl = vl;
    this.svg = svg;
    this.update();
  }
  update(){
    const [w, h] = [1, 1]; // aspect ratio
    const width = 200;
    const height = width * (h / w);
    const svg = this.svg;
    const vl = this.vl;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');
    vl.setAttributeNS(null, 'x1', width / 2);
    vl.setAttributeNS(null, 'y1', '0');
    vl.setAttributeNS(null, 'x2', width / 2);
    vl.setAttributeNS(null, 'y2', height);
    vl.setAttributeNS(null, 'stroke', width * 0.05);
    vl.setAttributeNS(null, 'stroke-width', '#eee');

    const segs = this.getSegments();
    const clipPaths = this.defs.querySelectorAll('clipPath');
    const kids = svg.querySelectorAll('g.steepness-segment');
    const maxlen = Math.max(segs.length, kids.length);
    let east = width * 0.5;
    let west = width * 0.5;
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];
      let clip = clipPaths[i];
      if (!seg) {
        kid.remove()
        continue;
      }
      if (!clip) {
        clip = document.createElementNS('http://www.w3.org/2000/svg', 'clipPath');
        this.defs.appendChild(clip);
        clip.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'circle'));
      }

      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        kid.setAttributeNS(null, 'class', 'steepness-segment');
        kid.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
        svg.appendChild(kid);
      }

      let p = kid.querySelector('path');
      let c = clip.querySelector('circle');

      const gap = height * 0.001;
      const south = height - (height * seg.start) - gap;
      const north = height - (height * seg.stop) + gap;
      const center = (north + south) / 2;
      const segLength = north - south;
      const radians = seg.radiansOverhung;
      const run = seg.run * segLength;
      const rise = seg.rise * segLength;

      const centerX = width * 0.5;
      const centerY = center;

      const southX = centerX - (run / 2);
      const southY = centerY - (rise / 2);
      const northX = centerX + (run / 2);
      const northY = centerY + (rise / 2);

      // 90deg from from the degree of overhang points directly into the wall 
      const perpendicularIntoWall = (seg.degreesOverhung + 90) * (Math.PI / 180);
      const offsetRun = Math.sin(perpendicularIntoWall) * segLength;
      const offsetRise = Math.cos(perpendicularIntoWall) * segLength;

      // offset each corner of the overhang to clip
      const southXo = southX - offsetRun;
      const southYo = southY - offsetRise;
      const northXo = northX - offsetRun;
      const northYo = northY - offsetRise;

      const id = `diagram-${this.id}-segment-${i}`;
      clip.setAttributeNS(null, 'id', id);
      kid.setAttributeNS(null, 'clip-path', `url(#${id})`);

      c.setAttributeNS(null, 'cx', centerX);
      c.setAttributeNS(null, 'cy', centerY);
      c.setAttributeNS(null, 'r',  (Math.abs(segLength) / 2));

      const d = 'M' + [[southXo, southYo],
                       [southX, southY],
                       [northX, northY],
                       [northXo, northYo]].map(([x, y]) => x + ' ' + y).join('L ') + 'Z';
      p.setAttributeNS(null, 'stroke', 'black');
      p.setAttributeNS(null, 'stroke-width', 1);
      p.setAttributeNS(null, 'fill', 'black');
      p.setAttributeNS(null, 'd', d);
    }
  }
}

export class Burst extends SteepnessSegments {
  constructor(segments){
    super();
    this.setSegments(segments);
    this.radius = 50;
    this.padding = this.radius / 10;
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const offset = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    offset.setAttribute('class', 'horizontal-offset');
    const center = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    center.setAttribute('class', 'center-point');
    const rays = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    rays.setAttribute('class', 'rays');
    const edge = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    edge.setAttribute('class', 'edge');
    svg.appendChild(offset);
    svg.appendChild(rays);
    svg.appendChild(center);
    svg.appendChild(edge);
    this.offset = offset;
    this.center = center;
    this.rays = rays;
    this.svg = svg;
    this.update();
  }
  update(){
    const radius = this.radius;
    const padding = this.padding;
    const width = (radius + padding) * 2;
    const height = (radius + padding) * 2;
    const svg = this.svg;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');
    const center = this.center;
    center.setAttributeNS(null, 'cx', (width/2));
    center.setAttributeNS(null, 'cy', (height/2));
    center.setAttributeNS(null, 'r', (radius * 0.15));
    center.setAttributeNS(null, 'style', 'stroke:none;fill:black;');
    const segs = this.getSegments();
    const kids = this.rays.querySelectorAll('g.ray');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];
      if (!seg) {
        kid.remove()
        continue;
      }
      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        kid.setAttributeNS(null, 'class', 'ray');
        let pct = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        pct.setAttributeNS(null, 'class', 'percentage');
        kid.appendChild(pct);
        this.rays.appendChild(kid);
      }
      const pct = kid.querySelector('line.percentage');
      pct.setAttributeNS(null, 'x1', (width/2));
      pct.setAttributeNS(null, 'y1', (height/2));
      pct.setAttributeNS(null, 'x2', (width/2) - (seg.run * seg.length * radius));
      pct.setAttributeNS(null, 'y2', (width/2) - (seg.rise * seg.length * radius));
      pct.setAttributeNS(null, 'stroke', 'black');
      pct.setAttributeNS(null, 'stroke-width', radius * 0.1);
    }
  }
}

export class BurstWithOpacityAndRing extends SteepnessSegments {
  constructor(segments){
    super();
    this.setSegments(segments);
    this.radius = 50;
    this.padding = this.radius / 10;
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const center = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    center.setAttribute('class', 'center-point');
    const rays = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    rays.setAttribute('class', 'rays');
    const edge = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    edge.setAttribute('class', 'edge');
    svg.appendChild(rays);
    svg.appendChild(center);
    svg.appendChild(edge);
    this.center = center;
    this.rays = rays;
    this.edge = edge;
    this.svg = svg;
    this.update();
  }
  update(){
    const radius = this.radius;
    const padding = this.padding;
    const width = (radius + padding) * 2;
    const height = (radius + padding) * 2;
    const svg = this.svg;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');
    const center = this.center;
    center.setAttributeNS(null, 'cx', (width/2));
    center.setAttributeNS(null, 'cy', (height/2));
    center.setAttributeNS(null, 'r', (radius * 0.1));
    center.setAttributeNS(null, 'style', 'stroke:none;fill:black;');
    const edge = this.edge;
    edge.setAttributeNS(null, 'cx', (width/2));
    edge.setAttributeNS(null, 'cy', (height/2));
    edge.setAttributeNS(null, 'r',  radius);
    edge.setAttributeNS(null, 'style', 'stroke:#ddd;stroke-width:1;fill:none;');
    const segs = this.getSegments();
    const kids = this.rays.querySelectorAll('g.ray');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];
      if (!seg) {
        kid.remove()
        continue;
      }
      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        kid.setAttributeNS(null, 'class', 'ray');
        let pct = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        pct.setAttributeNS(null, 'class', 'percentage');
        let dir = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        dir.setAttributeNS(null, 'class', 'direction');
        kid.appendChild(dir);
        kid.appendChild(pct);
        this.rays.appendChild(kid);
      }
      const pct = kid.querySelector('line.percentage');
      pct.setAttributeNS(null, 'x1', (width/2));
      pct.setAttributeNS(null, 'y1', (height/2));
      pct.setAttributeNS(null, 'x2', (width/2) - (seg.run * seg.length * radius));
      pct.setAttributeNS(null, 'y2', (width/2) - (seg.rise * seg.length * radius));
      pct.setAttributeNS(null, 'stroke', 'rgba(0, 0, 0, 0.5)');
      pct.setAttributeNS(null, 'stroke-linecap', 'round');
      pct.setAttributeNS(null, 'stroke-width', radius * 0.08);
    }
  }
}

export class BurstWithTicks extends SteepnessSegments {
  constructor(segments){
    super();
    this.setSegments(segments);
    this.radius = 50;
    this.padding = this.radius / 10;
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const center = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    center.setAttribute('class', 'center-point');
    const rays = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    rays.setAttribute('class', 'rays');
    const edge = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    edge.setAttribute('class', 'edge');
    svg.appendChild(rays);
    svg.appendChild(center);
    svg.appendChild(edge);
    this.center = center;
    this.rays = rays;
    this.edge = edge;
    this.svg = svg;
    this.update();
  }
  update(){
    const radius = this.radius;
    const padding = this.padding;
    const width = (radius + padding) * 2;
    const height = (radius + padding) * 2;
    const svg = this.svg;

    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');

    const center = this.center;
    center.setAttributeNS(null, 'cx', (width/2));
    center.setAttributeNS(null, 'cy', (height/2));
    center.setAttributeNS(null, 'r', (radius * 0.05));
    center.setAttributeNS(null, 'style', 'stroke:none;fill:black;');

    const edge = this.edge;
    edge.setAttributeNS(null, 'cx', (width/2));
    edge.setAttributeNS(null, 'cy', (height/2));
    edge.setAttributeNS(null, 'r',  radius);
    edge.setAttributeNS(null, 'style', 'stroke:#ddd;stroke-width:1;fill:none;');

    const segs = this.getSegments();
    const kids = this.rays.querySelectorAll('g.ray');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];

      if (!seg) {
        kid.remove()
        continue;
      }

      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        kid.setAttributeNS(null, 'class', 'ray');
        let pct = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        pct.setAttributeNS(null, 'class', 'percentage');
        let dir = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        dir.setAttributeNS(null, 'class', 'direction');
        kid.appendChild(dir);
        kid.appendChild(pct);
        this.rays.appendChild(kid);
      }

      const pct = kid.querySelector('line.percentage');
      const dir = kid.querySelector('line.direction');

      pct.setAttributeNS(null, 'x1', (width/2));
      pct.setAttributeNS(null, 'y1', (height/2));
      pct.setAttributeNS(null, 'x2', (width/2) - (seg.run * seg.length * radius));
      pct.setAttributeNS(null, 'y2', (width/2) - (seg.rise * seg.length * radius));
      pct.setAttributeNS(null, 'stroke', 'rgba(0, 0, 0, 0.5)');
      pct.setAttributeNS(null, 'stroke-linecap', 'round');
      pct.setAttributeNS(null, 'stroke-width', radius * 0.05);

      dir.setAttributeNS(null, 'x1', (width/2) - (seg.run * (radius - padding)));
      dir.setAttributeNS(null, 'y1', (height/2) - (seg.rise * (radius - padding)));
      dir.setAttributeNS(null, 'x2', (width/2) - (seg.run * (radius + padding)));
      dir.setAttributeNS(null, 'y2', (height/2) - (seg.rise * (radius + padding)));
      dir.setAttributeNS(null, 'stroke', '#ddd');
      dir.setAttributeNS(null, 'stroke-width', radius * 0.05);
    }
  }
}

export class BurstTrackingOverhang extends SteepnessSegments {
  constructor(segments){
    super();
    this.setSegments(segments);
    this.radius = 50;
    this.overhangBottomOffset = 0.25;
    this.padding = this.radius / 10;
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const offsetTrack = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    offsetTrack.setAttribute('class', 'horizontal-offset');
    const offsetMarker = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    offsetMarker.setAttribute('class', 'offset-marker');
    const center = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    center.setAttribute('class', 'center-point');
    const rays = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    rays.setAttribute('class', 'rays');
    const edge = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    edge.setAttribute('class', 'edge');
    svg.appendChild(offsetTrack);
    svg.appendChild(offsetMarker);
    svg.appendChild(rays);
    svg.appendChild(center);
    svg.appendChild(edge);
    this.offsetTrack = offsetTrack;
    this.offsetMarker = offsetMarker;
    this.center = center;
    this.rays = rays;
    this.edge = edge;
    this.svg = svg;
    this.update();
  }
  update(){
    const radius = this.radius;
    const padding = this.padding;
    const width = (radius + padding) * 2;
    const height = (radius + padding) * 2;
    const svg = this.svg;

    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');

    const center = this.center;
    center.setAttributeNS(null, 'cx', (width/2));
    center.setAttributeNS(null, 'cy', (height/2));
    center.setAttributeNS(null, 'r', (radius * 0.05));
    center.setAttributeNS(null, 'style', 'stroke:none;fill:black;');

    const edge = this.edge;
    edge.setAttributeNS(null, 'cx', (width/2));
    edge.setAttributeNS(null, 'cy', (height/2));
    edge.setAttributeNS(null, 'r',  radius);
    edge.setAttributeNS(null, 'style', 'stroke:#ddd;stroke-width:1;fill:none;');

    const segs = this.getSegments();
    const kids = this.rays.querySelectorAll('g.ray');
    const maxlen = Math.max(segs.length, kids.length);
    let totalRun = 0;
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];

      totalRun += (seg.length * seg.run * radius);

      if (!seg) {
        kid.remove()
        continue;
      }

      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        kid.setAttributeNS(null, 'class', 'ray');
        let pct = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        pct.setAttributeNS(null, 'class', 'percentage');
        let dir = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        dir.setAttributeNS(null, 'class', 'direction');
        kid.appendChild(dir);
        kid.appendChild(pct);
        this.rays.appendChild(kid);
      }

      const pct = kid.querySelector('line.percentage');
      const dir = kid.querySelector('line.direction');

      pct.setAttributeNS(null, 'x1', (width/2));
      pct.setAttributeNS(null, 'y1', (height/2));
      pct.setAttributeNS(null, 'x2', (width/2) - (seg.run * seg.length * radius));
      pct.setAttributeNS(null, 'y2', (width/2) - (seg.rise * seg.length * radius));
      pct.setAttributeNS(null, 'stroke', 'rgba(0, 0, 0, 0.5)');
      pct.setAttributeNS(null, 'stroke-linecap', 'round');
      pct.setAttributeNS(null, 'stroke-width', radius * 0.05);

      dir.setAttributeNS(null, 'x1', (width/2) - (seg.run * (radius - padding)));
      dir.setAttributeNS(null, 'y1', (height/2) - (seg.rise * (radius - padding)));
      dir.setAttributeNS(null, 'x2', (width/2) - (seg.run * (radius + padding)));
      dir.setAttributeNS(null, 'y2', (height/2) - (seg.rise * (radius + padding)));
      dir.setAttributeNS(null, 'stroke', '#ddd');
      dir.setAttributeNS(null, 'stroke-width', radius * 0.05);
    }
    const trk = this.offsetTrack;
    const trackY = height - (height * this.overhangBottomOffset);
    trk.setAttributeNS(null, 'x1', 0 + padding);
    trk.setAttributeNS(null, 'x2', width - padding);
    trk.setAttributeNS(null, 'y1', trackY);
    trk.setAttributeNS(null, 'y2', trackY);
    trk.setAttributeNS(null, 'style', 'stroke:gray;stroke-width:1');
    
    let mk = this.offsetMarker;
    mk.setAttributeNS(null, 'cx', (width/2) - totalRun);
    mk.setAttributeNS(null, 'cy', trackY);
    mk.setAttributeNS(null, 'r', 3);
    mk.setAttributeNS(null, 'style', 'stroke:gray;stroke-width:1:fill:gray');
  }
}

export class BurstAtEdge extends SteepnessSegments {
  constructor(segments){
    super();
    this.setSegments(segments);
    this.radius = 50;
    this.padding = this.radius / 10;
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const center = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    center.setAttribute('class', 'center-point');
    const rays = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    rays.setAttribute('class', 'rays');
    const edge = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    edge.setAttribute('class', 'edge');
    svg.appendChild(rays);
    svg.appendChild(center);
    svg.appendChild(edge);
    this.center = center;
    this.rays = rays;
    this.edge = edge;
    this.svg = svg;
    this.update();
  }
  update(){
    const radius = this.radius;
    const padding = this.padding;
    const width = (radius + padding) * 2;
    const height = (radius + padding) * 2;
    const svg = this.svg;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');
    const center = this.center;
    center.setAttributeNS(null, 'cx', (width/2));
    center.setAttributeNS(null, 'cy', (height/2));
    center.setAttributeNS(null, 'r', (radius * 0.1));
    center.setAttributeNS(null, 'style', 'stroke:none;fill:none;'); // for easy restoration
    const edge = this.edge;
    edge.setAttributeNS(null, 'cx', (width/2));
    edge.setAttributeNS(null, 'cy', (height/2));
    edge.setAttributeNS(null, 'r',  radius);
    edge.setAttributeNS(null, 'style', 'stroke:black;stroke-width:2;fill:none;');
    const segs = this.getSegments();
    const kids = this.rays.querySelectorAll('g.ray');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];
      if (!seg) {
        kid.remove()
        continue;
      }
      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        kid.setAttributeNS(null, 'class', 'ray');
        let pct = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        pct.setAttributeNS(null, 'class', 'percentage');
        let dir = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        dir.setAttributeNS(null, 'class', 'direction');
        kid.appendChild(dir);
        kid.appendChild(pct);
        this.rays.appendChild(kid);
      }

      const pct = kid.querySelector('line.percentage');
      pct.setAttributeNS(null, 'x1', (width/2) - (seg.run * (radius - (seg.length * radius))));
      pct.setAttributeNS(null, 'y1', (width/2) - (seg.rise * (radius - (seg.length * radius))));
      pct.setAttributeNS(null, 'x2', (width/2) - (seg.run * radius));
      pct.setAttributeNS(null, 'y2', (height/2) - (seg.rise * radius));
      pct.setAttributeNS(null, 'stroke', 'black');
      pct.setAttributeNS(null, 'stroke-linecap', 'round');
      pct.setAttributeNS(null, 'stroke-width', radius * 0.08);
    }
  }
}

export class Target extends SteepnessSegments {
  constructor(segments){
    super();
    this.setSegments(segments);
    this.radius = 50;
    this.padding = this.radius / 10;
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const center = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    center.setAttribute('class', 'center-point');
    const rays = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    rays.setAttribute('class', 'rays');
    const edge = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    edge.setAttribute('class', 'edge');
    svg.appendChild(rays);
    svg.appendChild(center);
    svg.appendChild(edge);
    this.center = center;
    this.rays = rays;
    this.edge = edge;
    this.svg = svg;
    this.update();
  }
  update(){
    const radius = this.radius;
    const padding = this.padding;
    const width = (radius + padding) * 2;
    const height = (radius + padding) * 2;
    const svg = this.svg;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');
    const center = this.center;
    center.setAttributeNS(null, 'cx', (width/2));
    center.setAttributeNS(null, 'cy', (height/2));
    center.setAttributeNS(null, 'r', (radius * 0.1));
    center.setAttributeNS(null, 'style', 'stroke:none;fill:none;'); // for easy restoration
    const edge = this.edge;
    edge.setAttributeNS(null, 'cx', (width/2));
    edge.setAttributeNS(null, 'cy', (height/2));
    edge.setAttributeNS(null, 'r',  radius);
    edge.setAttributeNS(null, 'style', 'stroke:black;stroke-width:2;fill:none;');
    const segs = this.getSegments();
    const kids = this.rays.querySelectorAll('g.ray');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];
      if (!seg) {
        kid.remove()
        continue;
      }
      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        kid.setAttributeNS(null, 'class', 'ray');
        let pct = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        pct.setAttributeNS(null, 'class', 'stretch');
        kid.appendChild(pct);
        this.rays.appendChild(kid);
      }
      const pct = kid.querySelector('line.stretch');
      pct.setAttributeNS(null, 'x1', (width/2) - (seg.run * seg.start * radius));
      pct.setAttributeNS(null, 'y1', (height/2) - (seg.rise * seg.start * radius));
      pct.setAttributeNS(null, 'x2', (width/2) - (seg.run * seg.stop * radius));
      pct.setAttributeNS(null, 'y2', (height/2) - (seg.rise * seg.stop * radius));
      pct.setAttributeNS(null, 'stroke', 'black');
//      pct.setAttributeNS(null, 'stroke-linecap', 'round');
      pct.setAttributeNS(null, 'stroke-width', radius * 0.08);
    }
  }
}

export class TargetWithRings extends SteepnessSegments {
  constructor(segments){
    super();
    this.setSegments(segments);
    this.radius = 50;
    this.padding = this.radius / 10;
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const center = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    center.setAttribute('class', 'center-point');
    const rays = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    rays.setAttribute('class', 'rays');
    const edge = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    edge.setAttribute('class', 'edge');
    svg.appendChild(rays);
    svg.appendChild(center);
    svg.appendChild(edge);
    this.center = center;
    this.rays = rays;
    this.edge = edge;
    this.svg = svg;
    this.update();
  }
  update(){
    const radius = this.radius;
    const padding = this.padding;
    const width = (radius + padding) * 2;
    const height = (radius + padding) * 2;
    const svg = this.svg;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');
    const center = this.center;
    center.setAttributeNS(null, 'cx', (width/2));
    center.setAttributeNS(null, 'cy', (height/2));
    center.setAttributeNS(null, 'r', (radius * 0.1));
    center.setAttributeNS(null, 'style', 'stroke:none;fill:none;'); // for easy restoration
    const edge = this.edge;
    edge.setAttributeNS(null, 'cx', (width/2));
    edge.setAttributeNS(null, 'cy', (height/2));
    edge.setAttributeNS(null, 'r',  radius);
    edge.setAttributeNS(null, 'style', 'stroke:none;stroke-width:2;fill:none;');
    const segs = this.getSegments();
    const kids = this.rays.querySelectorAll('g.ray');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];
      if (!seg) {
        kid.remove()
        continue;
      }
      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        kid.setAttributeNS(null, 'class', 'ray');
        let pct = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        pct.setAttributeNS(null, 'class', 'stretch');
        let end = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
        end.setAttributeNS(null, 'class', 'end');
        kid.appendChild(pct);
        kid.appendChild(end);
        this.rays.appendChild(kid);
      }
      const pct = kid.querySelector('line.stretch');
      pct.setAttributeNS(null, 'x1', (width/2) - (seg.run * seg.start * radius));
      pct.setAttributeNS(null, 'y1', (height/2) - (seg.rise * seg.start * radius));
      pct.setAttributeNS(null, 'x2', (width/2) - (seg.run * seg.stop * radius));
      pct.setAttributeNS(null, 'y2', (height/2) - (seg.rise * seg.stop * radius));
      pct.setAttributeNS(null, 'stroke', 'black');
      pct.setAttributeNS(null, 'stroke-width', radius * 0.1);
      const end = kid.querySelector('circle.end');
      end.setAttributeNS(null, 'cx', (width/2));
      end.setAttributeNS(null, 'cy', (height/2));
      end.setAttributeNS(null, 'r', (seg.stop * radius));
      end.setAttributeNS(null, 'fill', 'none');
      end.setAttributeNS(null, 'stroke', 'black');
      end.setAttributeNS(null, 'stroke-width', radius * 0.01);
    }
  }
}

export class ShadedBar extends SteepnessSegments {
  constructor(segments) {
    super();
    this.setSegments(segments);
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const baseColor = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    const srg = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    srg.setAttributeNS(null, 'class', 'segment-rects');
    svg.appendChild(srg);
    svg.appendChild(baseColor);
    this.baseColor = baseColor;
    this.srg = srg;
    this.svg = svg;
    this.update();
  }

  update(){
    const width = 50;
    const partition = 10;
    const height = 200;

    const maxSteep = 70;
    const maxSlab = -55;
    // const vertColorPadding = 5;
    const steepColor = 0;
    const vertColor = 125;
    const slabColor = 240;

    const svg = this.svg;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');
    const bc = this.baseColor;
    bc.setAttributeNS(null, 'x', 0);
    bc.setAttributeNS(null, 'y', 0);
    bc.setAttributeNS(null, 'width', partition);
    bc.setAttributeNS(null, 'height', height);
    bc.setAttributeNS(null, 'stroke', 'none');
    bc.setAttributeNS(null, 'fill', `rgb(${vertColor},${vertColor},${vertColor})`);

    const segs = this.getSegments();
    const kids = this.srg.querySelectorAll('rect.steepness-segment');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];

      if (!seg) {
        kid.remove()
        continue;
      }

      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        kid.setAttributeNS(null, 'class', 'steepness-segment');
        this.srg.appendChild(kid);
      }

      const south = height - (height * seg.start);
      const north = height - (height * seg.stop);
      const deg = seg.degreesOverhung;

      kid.setAttributeNS(null, 'x', 0);
      kid.setAttributeNS(null, 'y', north);
      kid.setAttributeNS(null, 'width', width);
      kid.setAttributeNS(null, 'height', seg.length * height);
      kid.setAttributeNS(null, 'stroke', 'none');

      if (deg >= 0) {
        let range = vertColor - steepColor;
        let offset = Math.min(deg, maxSteep) / maxSteep;
        let c = vertColor - (range * offset);
        kid.setAttributeNS(null, 'fill', `rgb(${c},${c},${c})`);
      } else {
        let range = slabColor - vertColor;
        let offset = Math.max(deg, maxSlab) / maxSlab;
        let c = vertColor + (range * offset);
        kid.setAttributeNS(null, 'fill', `rgb(${c},${c},${c})`);
      }
    }
  }
}

export class ColoredBlocks extends SteepnessSegments {

  constructor(segments) {
    super();
    this.setSegments(segments);
    this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.bg = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    this.svg.appendChild(this.bg);
    this.update();
  }

  randomBlockColor(seg){
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    const a = Math.floor(((Math.random()*0.75) + 0.25) * 100) / 100;
    return `rgb(${r},${g},${b},${a})`;
  }

  blockColor(seg){
    const deg = seg.degreesOverhung;
    const maxSlab = -45;
    const maxSteep = 90;
    if (0 > deg) {  // slab
      let o = deg / maxSlab;
      return `rgba(0, 255, 0, ${o})`;
//      return `rgba(36, 243, 79, ${o})`;
    } else {
      let o = deg / maxSteep;
      return `rgba(255,0,0,${o})`;
//      return `rgba(246,100,97,${o})`;
    }
// red   rgb(246,100,97,${o})
// green rgb(36, 243,79,${o})
  }


  update(){
    const width = 30;
    const height = 200;

    const svg = this.svg;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');

    const bg = this.bg;
    bg.setAttributeNS(null, 'x1', width/2);
    bg.setAttributeNS(null, 'y1', 0);
    bg.setAttributeNS(null, 'x2', width/2);
    bg.setAttributeNS(null, 'y2', height);
    bg.setAttributeNS(null, 'stroke', 'black');
    bg.setAttributeNS(null, 'stroke-width', width * 0.05);
    bg.setAttributeNS(null, 'stroke-opacity', 0.5);

    const segs = this.getSegments();
    const kids = this.svg.querySelectorAll('rect.steepness-block');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];

      if (!seg) {
        kid.remove()
        continue;
      }

      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        kid.setAttributeNS(null, 'class', 'steepness-block');
        this.svg.appendChild(kid);
      }

      const south = height - (height * seg.start);
      const north = height - (height * seg.stop);

      kid.setAttributeNS(null, 'x', 0);
      kid.setAttributeNS(null, 'y', north);
      kid.setAttributeNS(null, 'width', width);
      kid.setAttributeNS(null, 'height', seg.length * height);
      kid.setAttributeNS(null, 'stroke', 'none');
      kid.setAttributeNS(null, 'fill', this.blockColor(seg));
    }
  }
}

export class Flags extends SteepnessSegments {
  constructor(segments) {
    super();
    this.setSegments(segments);
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const flagpole = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    const frame = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    const flags = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    flags.setAttributeNS(null, 'class', 'segment-rects');
    svg.appendChild(flags);
    svg.appendChild(flagpole);
    svg.appendChild(frame);
    this.svg = svg;
    this.flagpole = flagpole;
    this.frame = frame;
    this.flags = flags;
    this.update();
  }

  update(){
    const width = 50;
    const flagpole = 2;
    const height = 200;

    const maxSteep = 70;
    const maxSlab = 55;

    const svg = this.svg;
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');

    const frame = this.frame;
    frame.setAttributeNS(null, 'x', 0);
    frame.setAttributeNS(null, 'y', 0);
    frame.setAttributeNS(null, 'width', width);
    frame.setAttributeNS(null, 'height', height);
    frame.setAttributeNS(null, 'fill', 'none');
    frame.setAttributeNS(null, 'stroke', 'black');
    frame.setAttributeNS(null, 'stroke-width', '1');
     

    const fp = this.flagpole;
    fp.setAttributeNS(null, 'x1', width/2);
    fp.setAttributeNS(null, 'y1', 0);
    fp.setAttributeNS(null, 'x2', width/2);
    fp.setAttributeNS(null, 'y2', height);
    fp.setAttributeNS(null, 'stroke', 'black');
    fp.setAttributeNS(null, 'stroke-width', flagpole);
    fp.setAttributeNS(null, 'fill', 'none');

    const segs = this.getSegments();
    const kids = this.flags.querySelectorAll('rect.steepness-segment');
    const maxlen = Math.max(segs.length, kids.length);
    for (let i = 0; i < maxlen; i++) {
      let seg = segs[i];
      let kid = kids[i];

      if (!seg) {
        kid.remove()
        continue;
      }

      if (!kid) {
        kid = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        kid.setAttributeNS(null, 'class', 'steepness-segment');
        this.flags.appendChild(kid);
      }

      const south = height - (height * seg.start);
      const north = height - (height * seg.stop);
      const deg = seg.degreesOverhung;

      const center = width/2;
      const maxWidth = width/2;

      const clamp = n => Math.min(n, 1);
      if (deg >= 0) { // steep
        const maxSteep = 60;
        const offset = maxWidth * clamp(deg/maxSteep);
        kid.setAttributeNS(null, 'x', center - offset);
        kid.setAttributeNS(null, 'y', north);
        kid.setAttributeNS(null, 'width', offset);
        kid.setAttributeNS(null, 'height', seg.length * height);
      } else if (0 > deg) { // slab
        const maxSlab = -45;
        const offset = maxWidth * clamp(deg/maxSlab);
        kid.setAttributeNS(null, 'x', center);
        kid.setAttributeNS(null, 'y', north);
        kid.setAttributeNS(null, 'width', offset);
        kid.setAttributeNS(null, 'height', seg.length * height);
      }
      kid.setAttributeNS(null, 'stroke', 'none');
      kid.setAttributeNS(null, 'fill', 'gray');
    }
  }
}
